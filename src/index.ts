import express, { application, Express, Request, Response } from 'express';
import paypal from 'paypal-rest-sdk';
import bodyParser from 'body-parser';
import mariadbConnection from './connections/mariadb';
import jwt from 'jsonwebtoken';
const cors = require('cors');

import indexRouter from './routes/index';

const app: Express = express();
const port: Number = 3006;

app.use(cors());

paypal.configure({
    'mode': 'sandbox',
    'client_id': process.env.PAYPAL_CLIENT_ID!,
    'client_secret': process.env.PAYPAL_CLIENT_SECRET!
});

app.use(bodyParser.json());
app.use('/', indexRouter);

app.set('db', mariadbConnection);
app.set('jwt', jwt);

app.listen(port, () => {
    console.log('[server]: The server is running at http://localhost:' + port);
});
