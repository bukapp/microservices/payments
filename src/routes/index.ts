import express, { Express, Request, Response, Router, NextFunction } from 'express';
import paypal, { Payment} from 'paypal-rest-sdk';
import middleware from '../modules/middleware';
import url from 'url';
var router: Router = express.Router()
const cors = require('cors');

router.use(middleware);

/* GET home page */
router.get('/error', function(req: Request, res: Response, next: NextFunction) {
    res.send('Error during the payment request');
});

router.get('/', async function(req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const results = await db.pool.query('select * from payments where user_id = ?', [req.body.user.id.toString()])
        res.send(results)
    } catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

router.options('/payment', cors());
router.get('/payment', async function(req: Request, res: Response, next: NextFunction) {
    try {

        var payment_json: Payment = {
            intent: 'sale',
            payer: { 
                payment_method: 'paypal'
            },
            redirect_urls: { 
                return_url: 'http://localhost:3006/finalize?jwttoken=' + req.query.jwttoken?.toString(),
                cancel_url: 'http://localhost:3006/error?jwttoken=' + req.query.jwttoken?.toString()
            },
            transactions: [{
                amount: {
                    total: req.query.amount?.toString()!,
                    currency: 'USD'
                },
                description: 'test payment'
            }]
        }
    
        paypal.payment.create(payment_json, function  (error, payment: any) {
            if (error) {
                throw error;
            } else {
                for(let i = 0;i < payment.links.length;i++){
                  if(payment.links[i].rel === 'approval_url'){
                    res.redirect(url.format({
                        pathname: payment.links[i].href,
                        query: req.query.jwttoken?.toString()
                    }));
                  }
                }
            }
          });
          
    } catch (err) {
        console.log(err);
        res.status(500).send({ error:  'Internal Server Error' });
    }

});

router.get('/finalize', function(req: Request, res: Response, next: NextFunction) {
    const db = req.app.get('db');

    const paymentId = req.query.paymentId!.toString();
    paypal.payment.get(paymentId, function(error, payment) {
        if(error) {
            console.log(error);
            throw error;
        } else {
            const results = db.pool.query("INSERT INTO payments (user_id, payment_id, content, address, amount) values (?,?,?,?,?)", 
            [
                req.body.user.id.toString(),
                paymentId,
                'no content',
                req.body.user.address.toString(),
                payment.transactions[0].amount.total
            ])
                .then( (resp: any) => {
                    console.log(resp);
                    res.send({
                        insertId: Number(resp.insertId)
                    });
                })
                .catch( (err: any) => {
                    console.log(err);
                    res.status(400).send(err.message);
                });
        }
     });

});

router.post('/', (req: express.Request, res: express.Response) =>{
    const db = req.app.get('db');
    console.log(req.body);
    try {
        const results = db.pool.query("INSERT INTO payments (user_id, payment_id, content, address, amount) values (?,?,?,?,?)", 
        [
            req.body.user.id.toString(),
            'fake paypal id',
            'no content',
            'address',
            req.body.amount.toString()
        ])
            .then( (resp: any) => {
                console.log(resp);
                res.send({
                    insertId: Number(resp.insertId)
                });
            })
            .catch( (err: any) => {
                console.log(err);
                res.status(400).send(err.message);
            });
    } catch (err) { console.log(err); }
});

export default router;
